package com.cwt.examination.rs;

import org.springframework.http.ResponseEntity;

import com.accounting.model.from.AccountingLineXYZ;
import com.accounting.model.to.AccountingLine;

public interface IAccountingRest {
	
	ResponseEntity<AccountingLine> processAccountingLine(AccountingLineXYZ al);
	
}