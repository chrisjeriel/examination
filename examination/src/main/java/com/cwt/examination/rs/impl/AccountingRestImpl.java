package com.cwt.examination.rs.impl;

import javax.ws.rs.Consumes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.accounting.model.from.AccountingLineXYZ;
import com.accounting.model.to.AccountingLine;
import com.cwt.examination.rs.IAccountingRest;
import com.cwt.examination.service.IAccountingService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/accounting")
public class AccountingRestImpl implements IAccountingRest {
	
	@Autowired
	IAccountingService accService;
	
	@PostMapping("/convert")
	@ApiOperation(value="Web Service Endpoint to process Accounting Line.", consumes = MediaType.APPLICATION_XML_VALUE)
	@Consumes(MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<AccountingLine> processAccountingLine(@RequestBody AccountingLineXYZ al) {
	  return ResponseEntity.ok(accService.processAccountingLine(al));
	}
}