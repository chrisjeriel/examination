package com.cwt.examination.service.impl;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accounting.model.from.AccountingLineXYZ;
import com.accounting.model.to.AccountingLine;
import com.cwt.examination.builder.AccountingClassBuilder;
import com.cwt.examination.service.IAccountingService;


@Service
public class AccountingServiceImpl implements IAccountingService{

	Logger logger = Logger.getLogger(AccountingServiceImpl.class.getName());

	@Autowired
	AccountingClassBuilder accBuilder;
	
	@Override
	public AccountingLine processAccountingLine(AccountingLineXYZ al) {
		return accBuilder.convertToAlABC(al);
	}
}
