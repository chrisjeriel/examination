package com.cwt.examination.service;

import com.accounting.model.from.AccountingLineXYZ;
import com.accounting.model.to.AccountingLine;

public interface IAccountingService {
	
	AccountingLine processAccountingLine(AccountingLineXYZ al);
	
}
