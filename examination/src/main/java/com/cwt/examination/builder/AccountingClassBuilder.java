package com.cwt.examination.builder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import com.accounting.model.from.AccountingLineXYZ;
import com.accounting.model.to.AccountingLine;
import com.accounting.model.to.AccountingLineStatus;
import com.accounting.model.to.MonetaryAmount;
import com.accounting.model.to.MonetaryPercentage;

@Component
public class AccountingClassBuilder{

	static final Logger logger = Logger.getLogger(AccountingClassBuilder.class.getName());
	boolean isParsedSuccessful;
	
	public AccountingLine convertToAlABC(AccountingLineXYZ alXYZ) {
		AccountingLine al = new AccountingLine();
		isParsedSuccessful = true;
		try {
			al.setAccountingLineID(alXYZ.getId());
			al.setAccountingVendorCode(alXYZ.getAccountingVendorCode());
			al.setAirlineCode(null);
			al.setChargeCategoryCode(alXYZ.getChargeCategoryCoded());
			al.setFormattedReceiptNumber(null);
			al.setInvoiceNumber(alXYZ.getOriginalInvoice());
			al.setLinkCode(alXYZ.getLinkCode());
			al.setNumberOfConjunctedDocuments(checkIfInvalidConjDocument(alXYZ.getNumberOfConjunctedDocuments()));
			al.setOriginalTicketNumber(alXYZ.getOriginalTicketNumber());
			al.setReceiptNumber(alXYZ.getDocumentNumber());
			
			List<String> segRefIdList = new ArrayList<>();
			segRefIdList.add(alXYZ.getSegmentNumber());
			al.setSegmentRefIDList(segRefIdList);
			
			al.setTravelerName(alXYZ.getPassengerName());
			
			List<String> travRefIdList = new ArrayList<>();
			travRefIdList.add("1");
			al.setTravelerRefIDList(travRefIdList);
			
			al.setTypeIndicator(alXYZ.getTypeIndicator());
			al.setElementNumber(alXYZ.getIndex().toString());
			
			al.setFareApplication(alXYZ.getFareApplication());
			
			al.setBaseFare(maParser(alXYZ.getBaseFare(), "BaseFare"));  //AccountingLine.XYZ/BaseFare
			al.setTaxAmount(maParser(alXYZ.getTaxAmount(), "TaxAmount")); //AccountingLine.XYZ/TaxAmount
			al.setTotalTaxSurcharge(maParser(bdAdditionToString(alXYZ.getGSTAmount(), alXYZ.getQSTAmount(), "TotalTaxSurcharge"), "TotalTaxSurcharge")); //GSTAmount + QSTAmount
			al.setTotalTaxAmount(maParser(bdAdditionToString(alXYZ.getTaxAmount(), al.getTotalTaxSurcharge().getFormattedValue(), "TotalTaxAmount"), "TotalTaxAmount")); //TaxAmount + totalTaxSurcharge
			al.setGstAmount(maParser(alXYZ.getGSTAmount(), "GstAmount")); //AccountingLine.XYZ/GSTAmount
			al.setGstCode(alXYZ.getQSTCode());
			al.setQstAmount(maParser(alXYZ.getQSTAmount(), "QstAmount")); //AccountingLine.XYZ/QSTAmount
			al.setQstCode(alXYZ.getGSTCode());
			
			MonetaryPercentage mp = new MonetaryPercentage();
			mp.setAmount(maParser(alXYZ.getCommissionPercentage(), "Commision-Amount"));
			mp.setPercentage(new BigDecimal(alXYZ.getCommissionAmount()));
			
			al.setCommission(mp);
			al.setFreeFormText(alXYZ.getFreeFormText());
		} catch (Exception e) {
			logger.warning("General exception occured." + e.getLocalizedMessage());
			this.isParsedSuccessful = false;
		} finally {
			if (isParsedSuccessful) {
				al.setAccountingLineStatus(AccountingLineStatus.ACTIVE);
			} else {
				al.setAccountingLineStatus(AccountingLineStatus.INACTIVE);
			}
		}
		
		return al;
	}
	
	
	public MonetaryAmount maParser(String val, String fieldName) {
		MonetaryAmount ma = new MonetaryAmount();
		try {
			BigDecimal bdVal = new BigDecimal(val);
			ma.setFormattedValue(val);
			ma.setValue((long)Double.parseDouble(val));
			ma.setNumberOfDecimals(getNumberOfDecimalPlaces(bdVal));
		} catch (NumberFormatException ex) {
			logger.warning("[maParser] - NFE Unable to parse value for field [" + fieldName + "].");
			this.isParsedSuccessful = false;
		}
		
		return ma;
	}
	
	int getNumberOfDecimalPlaces(BigDecimal bigDecimal) {
	    String string = bigDecimal.stripTrailingZeros().toPlainString();
	    int index = string.indexOf('.');
	    return index < 0 ? 0 : string.length() - index - 1;
	}
	
	public String bdAdditionToString(String val1, String val2, String fieldName){
		
		try {
			BigDecimal bdVal1 = new BigDecimal(val1);
			BigDecimal bdVal2 = new BigDecimal(val2); 

			return bdVal1.add(bdVal2).toString();
		} catch (NumberFormatException e) {
			logger.warning("[bdAdditionToString] - NFE Unable to parse value for field [" + fieldName + "].");
			this.isParsedSuccessful = false;
		} catch (NullPointerException e) {
			logger.warning("[bdAdditionToString] - NPE Unable to parse value for field [" + fieldName + "].");
			this.isParsedSuccessful = false;
		}
		return "";
	}
	
	/*public String checkIfInvalidConjDocument2(String val) {
		String[] str =  val.split("-");
		
		if (str.length > 0) {
			boolean isNumeric = str[0].chars().allMatch( Character::isDigit );
			if (!isNumeric || str[0].length() == 0) {
				logger.warning("[checkIfValidConjDocument] - Invalid value for field [NumberOfConjunctedDocuments].");
				this.isParsedSuccessful = false;
			}
		}
		
		return val;
	}*/
	
	
	/**
	 * @param str
	 * @return str
	 * regex pattern of 1 or more digits followed by "-" and any character 
	 * ie. "2-NN" "15-FEE-EXP$08"
	 */
	public String checkIfInvalidConjDocument(String str) {
		if(!str.matches("^\\d+-.+")) {
			logger.warning("Invalid value for field [NumberOfConjunctedDocuments]: " + str);
			this.isParsedSuccessful = false;
		}
		return str;
	}
	
	public boolean checkIfInvalidConjDocument2(String str) {
		if(!str.matches("^\\d+-.+")) {
			logger.warning("Invalid value for field [NumberOfConjunctedDocuments]: " + str);
			return false;
		}
		return true;
	}

}
