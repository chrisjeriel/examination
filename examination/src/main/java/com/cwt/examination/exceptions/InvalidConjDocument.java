package com.cwt.examination.exceptions;

public class InvalidConjDocument extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidConjDocument(String errorMessage) {
        super(errorMessage);
    }
}
