package com.cwt.examination.exceptions;

public class ErrorOnParsing extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ErrorOnParsing(String errorMessage) {
        super(errorMessage);
    }
	
}
