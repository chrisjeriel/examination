package com.cwt.examination;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.accounting.model.from.AccountingLineXYZ;
import com.accounting.model.to.AccountingLine;
import com.accounting.model.to.MonetaryAmount;
import com.cwt.examination.builder.AccountingClassBuilder;

@RunWith(MockitoJUnitRunner.class)
public class ExaminationApplicationTests {

	@InjectMocks
	private AccountingClassBuilder sut;

	AccountingLineXYZ xyz;

	@Before
	public void setup() {
		xyz = new AccountingLineXYZ();

		xyz.setNumberOfConjunctedDocuments("2-NN");

		xyz.setTypeIndicator("TYPE-01");
		xyz.setFareApplication("ONE");
		xyz.setFormOfPaymentCode("FormOfPaymentCode0");
		xyz.setLinkCode("01");
		xyz.setAccountingVendorCode("ABC");
		xyz.setChargeCategoryCoded("TEST");
		xyz.setAirlineDesignator("AA");
		xyz.setDocumentNumber("3889081143");
		xyz.setCommissionPercentage("2");
		xyz.setCommissionAmount("2");
		xyz.setBaseFare("1252.00");
		xyz.setBasePercent("");
		xyz.setTaxPercentage("1");
		xyz.setTaxAmount("201.38");
		xyz.setTaxSurchargeCode2("VAT");
		xyz.setGSTCode("GST");
		xyz.setGSTAmount("123.10");
		xyz.setGSTPercent("1");
		xyz.setQSTCode("QST");
		xyz.setQSTAmount("283.20");
		xyz.setQSTPercent("1");
		xyz.setCreditCardNumber("123456789");
		xyz.setCreditCardCode("AX0");
		xyz.setPassengerName("Thanos");
		xyz.setNumberOfCoupons("1");
		xyz.setOriginalTicketNumber("123456");
		xyz.setOriginalDateOfIssue("2021-01-28");
		xyz.setOriginalPlaceOfIssue("MNL");
		xyz.setFullPartialExchangeIndicator("PART");
		xyz.setOriginalInvoice("00001");
		xyz.setTarriffBasis("TarriffBasis0");
		xyz.setFreeFormText("HELLO WORLD");
		xyz.setCurrencyCode("USD");
		xyz.setSegmentType("AIR");
		xyz.setSegmentNumber("1");
		xyz.setId("572");
		xyz.setElementId("PNR-572");
		xyz.setIndex(1);
	}

	@Test
	public void testABCEqualXYZ() {
		assertNotNull(sut);

		AccountingLine abc = sut.convertToAlABC(xyz);

		assertEquals(abc.getAccountingLineID(), xyz.getId());
		assertEquals(abc.getAccountingVendorCode(), xyz.getAccountingVendorCode());
		assertNull(abc.getAirlineCode());
		assertEquals(abc.getChargeCategoryCode(), xyz.getChargeCategoryCoded());
		assertNull(abc.getFormattedReceiptNumber());
		assertEquals(abc.getInvoiceNumber(), xyz.getOriginalInvoice());
		assertEquals(abc.getLinkCode(), xyz.getLinkCode());
		assertEquals(abc.getNumberOfConjunctedDocuments(), xyz.getNumberOfConjunctedDocuments());
		assertEquals(abc.getOriginalTicketNumber(), xyz.getOriginalTicketNumber());
		assertEquals(abc.getReceiptNumber(), xyz.getDocumentNumber());

		List<String> segRefList = new ArrayList<>();
		segRefList.add(xyz.getSegmentNumber());
		assertEquals(abc.getSegmentRefIDList(), segRefList);

		assertEquals(abc.getTravelerName(), xyz.getPassengerName());

		List<String> travRefIdList = new ArrayList<>();
		travRefIdList.add("1");
		assertEquals(abc.getTravelerRefIDList(), travRefIdList);

		assertEquals(abc.getTypeIndicator(), xyz.getTypeIndicator());
		assertEquals(abc.getElementNumber(), xyz.getIndex().toString());
		assertEquals(abc.getFareApplication(), xyz.getFareApplication());
		assertEquals(abc.getBaseFare().getFormattedValue(), xyz.getBaseFare());
		assertEquals(abc.getTaxAmount().getFormattedValue(), xyz.getTaxAmount());
		// assertEquals(abc.getTotalTaxAmount(), xyz.);
		// assertEquals(abc.getTotalTaxSurcharge(), xyz.);
		assertEquals(abc.getGstAmount().getFormattedValue(), xyz.getGSTAmount());
		assertEquals(abc.getGstCode(), xyz.getQSTCode());
		assertEquals(abc.getQstAmount().getFormattedValue(), xyz.getQSTAmount());
		assertEquals(abc.getQstCode(), xyz.getGSTCode());

		assertNotNull(abc.getCommission());
		assertEquals(abc.getCommission().getAmount().getFormattedValue(), xyz.getCommissionAmount());
		assertEquals(abc.getCommission().getPercentage().toString(), xyz.getCommissionPercentage());
		assertEquals(abc.getFreeFormText(), xyz.getFreeFormText());

	}

	@Test
	public void testCheckIfInvalidConjDocument2() {

		assertTrue(sut.checkIfInvalidConjDocument2("2-NN"));
		assertTrue(sut.checkIfInvalidConjDocument2("15-FEE-EXP$08"));
		assertTrue(sut.checkIfInvalidConjDocument2("3599-NKAJSHDKAH"));
		assertTrue(sut.checkIfInvalidConjDocument2("0081181-NNXX"));
		assertTrue(sut.checkIfInvalidConjDocument2("998-!@#!$%!---"));
		assertFalse(sut.checkIfInvalidConjDocument2("!2-X1`23"));
		assertFalse(sut.checkIfInvalidConjDocument2("X2-x123"));
		assertFalse(sut.checkIfInvalidConjDocument2("@#2-123"));
		assertFalse(sut.checkIfInvalidConjDocument2("NN2-55123"));
		assertFalse(sut.checkIfInvalidConjDocument2("-882-55123"));
		
	}
	
	@Test
	public void testMaParser() {

		MonetaryAmount maTest1 = sut.maParser("1005.20", "TestField1");
		
		assertEquals(maTest1.getFormattedValue(), "1005.20");
		
	}

}
