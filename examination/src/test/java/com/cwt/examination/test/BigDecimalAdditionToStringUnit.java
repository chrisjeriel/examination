//package com.cwt.examination.test;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//import org.junit.jupiter.api.Test;
//
//import com.cwt.examination.service.IAccountingService;
//
//class BigDecimalAdditionToStringUnit {
//
//	@Test
//	void test() {
//		IAccountingService obj1 = new IAccountingService();
//		
//		String test1 = obj1.bdAdditionToString("123.10", "283.20", "SampleField1");
//		assertEquals("406.30", test1);
//		
//		String test2 = obj1.bdAdditionToString("10", "10", "SampleField1");
//		assertEquals("20", test2);
//		
//		String test3 = obj1.bdAdditionToString("11", "10", "SampleField1");
//		assertEquals("21", test3);
//	}
//	
//}
